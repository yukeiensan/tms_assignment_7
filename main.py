#!/usr/bin/env python3

import argparse
import math
import csv


# Project functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Mortgage amortization schedule generator")
    parser.add_argument("principal", help="Mortgage amount")
    parser.add_argument("interest_rate", help="Mortgage interest rate")
    parser.add_argument("months", help="Number of months")
    args = parser.parse_args()

    return args


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n * multiplier + 0.5) / multiplier


def calculate(principal, interest_rate, months):
    interest_rate /= 100
    total_payment = round_half_up(principal * interest_rate * pow(1+interest_rate, months) /
                        (pow(1+interest_rate, months) - 1), 2)
    total_interest = 0
    loan = principal
    print("Month\t\tPrincipal\tInterest\tTotal Interest\tLoan Balance")
    for i in range(0, months, 1):
        interest = round_half_up(loan * interest_rate, 2)
        monthly_principal = round_half_up(total_payment - interest, 2)
        total_interest = round_half_up(total_interest + interest, 2)
        loan = round_half_up(loan - monthly_principal, 2)
        print(str(i + 1) + "\t\t" + str(monthly_principal) + "\t\t" + str(interest) + "\t\t" + str(total_interest)
              + "\t\t\t" + str(loan))


def main():
    args = parsing_arguments()
    principal = float(args.principal)
    interest_rate = float(args.interest_rate)
    months = int(args.months)
    calculate(principal, interest_rate, months)

if __name__ == "__main__":
    main()