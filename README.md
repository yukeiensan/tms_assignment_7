# Installing requirements
`None`

# Usage
```
usage: amortization_schedule.exe [-h] principal interest_rate months

Mortgage amortization schedule generator

positional arguments:
  principal      Mortgage amount
  interest_rate  Mortgage interest rate
  months         Number of months

optional arguments:
  -h, --help     show this help message and exit

```
